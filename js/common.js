$(function() {
	$('.table').on('click', '.desc > span', function() {
		$('.desc').removeClass('selected');
		$(this).parent().addClass('selected');
		var w = $(this).find('.w').text(),
			h = $(this).find('.h').text();
		$('.example').addClass('selected');
		/* $('.sq').css({'width': w, 'height': h}); */
	});
	$('.close').on('click', function() {
		$('.desc').removeClass('selected');
		$('.example').removeClass('selected');
	});
	$('.desc td').hover(function() {
		clearTimeout($.data(this,'timer'));
		var ind = $(this).index() + 1;
		if(ind == 4 || ind == 5) {
			$('.desc th:nth-child(' + (ind - 1) + '), .desc td:nth-child(' + ind + ')').addClass('active');
		}
		else $('.desc td:nth-child(' + ind + '), .desc th:nth-child(' + ind + ')').addClass('active');
		setTimeout(function() {
			$('.active').on("mouseleave", function() {
				$('.desc td:nth-child(' + ind + '), .desc th:nth-child(' + ind + ')').removeClass('active');
			});
		}, 0);
	});
	$('.desc th').hover(function() {
		clearTimeout($.data(this,'timer'));
		var ind = $(this).index() + 1;
		if(ind == 3) {
			$('.desc td:nth-child(' + ind + '), .desc th:nth-child(' + ind + ')').addClass('active');
			$('.desc td:nth-child(' + (ind + 1) + '), .desc th:nth-child(' + ind + ')').addClass('active');
		}
		else if(ind == 4) {
			$('.desc td:nth-child(' + (ind + 1) + '), .desc th:nth-child(' + ind + ')').addClass('active');
		}
		else $('.desc td:nth-child(' + ind + '), .desc th:nth-child(' + ind + ')').addClass('active');
		setTimeout(function() {
			$('.active').on("mouseleave", function() {
				$('.desc td:nth-child(' + ind + '), .desc th:nth-child(' + ind + ')').removeClass('active');
			});
		}, 0);
	});
	$('.extended').on('click', 'td:not(:last-child)', function() {
		$('.hidden').slideToggle();
	});
	$('.fix_block ul').hover(function () {
		clearTimeout($.data(this,'timer'));
		$('.menu_header').hide();
		$('.fix_block .item').slideDown();
		}, function () {
			$.data(this,'timer', setTimeout($.proxy(function() {
			$('.fix_block .item').slideUp();
			setTimeout(function(){$('.menu_header').show()}, 400);
		}, this), 300));
    });
	$('.ar_header').on('click', function() {
		$('.archive > div').slideToggle();
		initSliders();
	});
	
	function initSliders() {
		$('.slider-for').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  arrows: false,
		  asNavFor: '.slider-nav'
		});
		$('.slider-nav').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  asNavFor: '.slider-for'
		});
		$('.slider-for-m').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  arrows: false,
		  asNavFor: '.slider-nav-m'
		});
		$('.slider-nav-m').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  asNavFor: '.slider-for-m'
		});
	}
});